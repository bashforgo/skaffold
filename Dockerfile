# Download docker
FROM docker:19.03.8 as download-docker

# Download kubectl
FROM alpine:3.11 as download-kubectl
ENV KUBECTL_VERSION v1.17.3
ENV KUBECTL_URL https://dl.k8s.io/${KUBECTL_VERSION}/kubernetes-client-linux-amd64.tar.gz
RUN wget -O kubectl.tar.gz "${KUBECTL_URL}"
RUN tar -xvf kubectl.tar.gz --strip-components 3

# Download helm
FROM alpine:3.11 as download-helm
ENV HELM_VERSION v3.1.1
ENV HELM_URL https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz
RUN wget -O helm.tar.gz "${HELM_URL}"
RUN tar -xvf helm.tar.gz --strip-components 1

# Download skaffold
FROM alpine:3.11 as download-skaffold
ENV SKAFFOLD_VERSION v1.5.0
ENV SKAFFOLD_URL https://storage.googleapis.com/skaffold/releases/${SKAFFOLD_VERSION}/skaffold-linux-amd64
RUN wget -O skaffold "${SKAFFOLD_URL}"
RUN chmod +x skaffold

FROM alpine:3.11
COPY --from=download-docker /usr/local/bin/docker /usr/local/bin/
COPY --from=download-kubectl kubectl /usr/local/bin/
COPY --from=download-helm helm /usr/local/bin/
COPY --from=download-skaffold skaffold /usr/local/bin/

RUN apk --update add git less openssh && \
    rm -rf /var/lib/apt/lists/* && \
    rm /var/cache/apk/*

LABEL version="v1.5.0"
